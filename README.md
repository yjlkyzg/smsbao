# 短信宝php扩展包

[![Latest Stable Version](http://poser.pugx.org/le/smsbao/v)](https://packagist.org/packages/le/smsbao) [![Total Downloads](http://poser.pugx.org/le/smsbao/downloads)](https://packagist.org/packages/le/smsbao) [![Latest Unstable Version](http://poser.pugx.org/le/smsbao/v/unstable)](https://packagist.org/packages/le/smsbao) [![License](http://poser.pugx.org/le/smsbao/license)](https://packagist.org/packages/le/smsbao) [![PHP Version Require](http://poser.pugx.org/le/smsbao/require/php)](https://packagist.org/packages/le/smsbao)

#### 介绍
1. 国内短信单发
2. 国内短信群发
3. 国际短信单发
4. 国际短信群发
5. 语音验证码发送

#### 使用说明

1. 使用composer拉取代码 composer require le/smsbao  
2. 查看test/test.php 使用示例

#### 示例

```
use Le\Smsbao\SmsBao;

$config = [
    'user'=>'xxxx',//短信宝平台账号
    'pass'=>'xxxx' //短信宝平台密码或者apikey
];


$smsbao = new SmsBao($config);


//国内短信单发
$res = $smsbao->sms('181xxx',"【测试内容】您的验证码是1234。如非本人操作，请忽略本短信");

//国内短信群发(单次不可超过99个号码)
$phones = ['181xxxx','182xxxx'];
$res = $smsbao->sms($phones,"【测试内容】您的验证码是1234。如非本人操作，请忽略本短信");


//国际短信单发
$res = $smsbao->wsms('+1xxxx',"【测试内容】您的验证码是1234。如非本人操作，请忽略本短信");

//国际短信群发(单次不可超过99个号码)
$wphones = ['+1181xxxx','+1182xxxx'];
$res = $smsbao->wsms($wphones,"【测试内容】您的验证码是1234。如非本人操作，请忽略本短信");

//发送语音验证码
$res = $smsbao->voice('181xxx',1234);
```



[短信宝网址https://www.smsbao.com/](https://www.smsbao.com/)