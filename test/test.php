<?php

use Le\Smsbao\SmsBao;

require '../src/SmsBao.php';
require '../src/SmsBaoException.php';

$config = [
    'user'=>'xxxx',//短信宝平台账号
    'pass'=>'xxxx' //短信宝平台密码或者apikey
];

$smsbao = new SmsBao($config);


//国内短信单发
$res = $smsbao->sms('181xxx',"【测试内容】您的验证码是1234。如非本人操作，请忽略本短信");

//国内短信群发(单次不可超过99个号码)
$phones = ['181xxxx','182xxxx'];
$res = $smsbao->sms($phones,"【测试内容】您的验证码是1234。如非本人操作，请忽略本短信");


//国际短信单发
$res = $smsbao->wsms('+1xxxx',"【测试内容】您的验证码是1234。如非本人操作，请忽略本短信");

//国际短信群发(单次不可超过99个号码)
$wphones = ['+1181xxxx','+1182xxxx'];
$res = $smsbao->wsms($wphones,"【测试内容】您的验证码是1234。如非本人操作，请忽略本短信");

//发送语音验证码
$res = $smsbao->voice('181xxx',1234);

//获取当前余额
$res = $smsbao->getBalance();


// 成功返回
// array(2) {
//     ["code"]=>
//     string(1) "0"
//     ["msg"]=>
//     string(18) "短信发送成功"
//   }

// 失败返回
// array(2) {
//     ["code"]=>
//     string(2) "51"
//     ["msg"]=>
//     string(21) "手机号码不正确"
//   }

