<?php
// +----------------------------------------------------------------------
// | 短信宝扩展
// +----------------------------------------------------------------------
// | 官方网站: https://gitee.com/yjlkyzg
// +----------------------------------------------------------------------
// | gitee 仓库地址 ：https://gitee.com/yjlkyzg/smsbao
// +----------------------------------------------------------------------
// | Author: Le
// +----------------------------------------------------------------------
// | Date: 2022.12.24
// +----------------------------------------------------------------------

namespace Le\Smsbao;

use Throwable;

class SmsBao{


    private $url = 'https://api.smsbao.com/';

    private $config = [
        'user'=>'',
        'pass'=>''
    ];

    public $statusStr = array(
        "0" => "短信发送成功",
        "-1" => "参数不全",
        "-2" => "服务器空间不支持,请确认支持curl或者fsocket,联系您的空间商解决或者更换空间!",
        "30" => "密码错误",
        "40" => "账号不存在",
        "41" => "余额不足",
        "42" => "帐户已过期",
        "43" => "IP地址限制",
        "50" => "内容含有敏感词",
        "51" => "手机号码不正确",
    );



    public function __construct($config)
    {
        $this->config = $config;
    }


    //检查配置
    function checkConfig(){

        if(empty($this->config['user'])) throw new SmsBaoException('未配置短信宝账号');
        if(empty($this->config['pass'])) throw new SmsBaoException('未配置短信宝密码');

    }

    /**
     * 发送国内短信
     * @param string|array $iphone 手机号码 (群发传数组格式手机号)
     * @param string $content 短信内容
     * @return array
     */
    public function sms($phone,$content){

        $this->checkConfig();

        if(is_array($phone)){

            if(count($phone) > 99){
                throw new SmsBaoException('群发单次最多不可超过99个号码');
            }

            $phone = implode(",",$phone);
        }

        try{

            $sendurl = $this->url."sms?u=".$this->config['user']."&p=".md5($this->config['pass'])."&m=".$phone."&c=".urlencode($content);
            $result =file_get_contents($sendurl) ;

        }catch(Throwable $e){
            throw new SmsBaoException('发送失败:'.$e->getMessage());
        }

        return ['code'=>$result,'msg'=>$this->statusStr[$result]];
    }

    /**
     * 发送国际短信
     * @param string|array $iphone 手机号码 (群发传数组格式手机号)
     * @param string $content 短信内容
     * @return array
     */
    public function wsms($phone,$content){

        $this->checkConfig();

        if(is_array($phone)){

            if(count($phone) > 99){
                throw new SmsBaoException('群发单次最多不可超过99个号码');
            }

            $phone = array_map(function($item){
                return urlencode($item);
            },$phone);

            $phone = implode(",",$phone);
        }else{
            $phone = urlencode($phone);
        }

        try{

            $sendurl = $this->url."wsms?u=".$this->config['user']."&p=".md5($this->config['pass'])."&m=".$phone."&c=".urlencode($content);
            $result =file_get_contents($sendurl) ;


        }catch(Throwable $e){
            throw new SmsBaoException('发送失败:'.$e->getMessage());
        }

        return ['code'=>$result,'msg'=>$this->statusStr[$result]];
    }

    /**
     * 发送语音验证码
     * @param string $iphone 手机号码
     * @param int $code 验证码
     * @return array
     */
    public function voice($phone,$code){

        /**
         *注意事项
         *1.同一被叫号码30秒内最多发送 1 条。
         *2.同一被叫号码10分钟内最多发送 2 条。
         *3.同一被叫号码1天内最多发送3条。
        */

        $this->checkConfig();

        try{

            $sendurl = $this->url."wsms?u=".$this->config['user']."&p=".md5($this->config['pass'])."&m=".$phone."&c=".$code;
            $result =file_get_contents($sendurl) ;


        }catch(Throwable $e){
            throw new SmsBaoException('发送失败:'.$e->getMessage());
        }

        return ['code'=>$result,'msg'=>$this->statusStr[$result]];
    }


   
    /**
     * 获取当前账号余额
     * @return int
     */
    public function getBalance(){

        $this->checkConfig();

        try{

            $res = file_get_contents($this->url . "query?u=" . $this->config['user'] . "&p=" . md5($this->config['pass']));

        }catch(Throwable $e){
            throw new SmsBaoException('获取失败:'.$e->getMessage());
        }

        return $res;
    }

}